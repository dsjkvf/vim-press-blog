vim-press-blog
==============


This is a fork of [VimBlog](https://github.com/danielmiessler/VimBlog), which was some fork of [VimRepress](http://www.vim.org/scripts/script.php?script_id=3510). README.orig can be found [here](https://bitbucket.org/dsjkvf/vim-press-blog/src/master/README.orig).
