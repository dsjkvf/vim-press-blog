" Init
if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

" Source Markdown (and HTML) elements
runtime! syntax/mkd.vim
unlet b:current_syntax

" Introduce new elements
sy match blogeditorEntry      "^ *[0-9]*\t.*$"
sy match blogeditorComment    '^".*$'
sy match blogeditorIdent      '^"[^:]*:'
hi link blogeditorEntry       Title
hi link blogeditorComment     Comment
hi link blogeditorIdent       Comment

" Fini
let b:current_syntax = "blog"
