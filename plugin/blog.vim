" HEADER

" License:
" ########################################################################
"
"  Copyright (C) 2007 Adrien Friggeri.
"
"  This program is free software; you can redistribute it and/or modify
"  it under the terms of the GNU General Public License as published by
"  the Free Software Foundation; either version 2, or (at your option)
"  any later version.
"
"  This program is distributed in the hope that it will be useful,
"  but WITHOUT ANY WARRANTY; without even the implied warranty of
"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"  GNU General Public License for more details.
"
"  You should have received a copy of the GNU General Public License
"  along with this program; if not, write to the Free Software Foundation,
"  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
"
" ########################################################################

" Credits:
" ########################################################################
"
"  LEGACY:
"  CONTRIBUTORS:     Adrien Friggeri <adrien@friggeri.net>
"                    Pigeond <http://pigeond.net/blog/>
"                    Justin Sattery <justin.slattery@fzysqr.com>
"                    Lenin Lee <lenin.lee@gmail.com>
"                    Conner McDaniel <connermcd@gmail.com>
"                    John Calhoun <johnmcalhoun123@gmail.com>
"
"  FORKED BY:        Preston M.[BOYPT] <pentie@gmail.com>
"  REPOSITORY:       https://bitbucket.org/pentie/vimrepress
"
"  URL:              http://www.friggeri.net/projets/vimblog/
"                    http://pigeond.net/blog/2009/05/07/vimpress-again/
"                    http://pigeond.net/git/?p=vimpress.git
"                    http://fzysqr.com/
"                    http://apt-blog.net
"  CURRENT:
"  MAINTAINER:       dsjkvf <dsjkvf@gmail.com>
"  URL:              https://bitbucket/dsjkvf/vim-press-blog/
"  VERSION:          4.0.0
"
" ########################################################################

" Configuration:
"  danielmiessler-vimblog:
"     - A mod of a mod of a mod of Vimpress
"     - A vim plugin fot writting your wordpress blog
"     - Write with Markdown, control posts format precisely
"     - Stores Markdown rawtext in wordpress custom fields
"
"  configure ~/.config/vimblog/rc in the following manner:
"
"  [Blog0]
"  blog_url = http://a-blog.com/
"  username = admin
"  password = 123456
"
"  [Blog1]
"  blog_url = https://someone.wordpress.com/
"  username = someone
"  password =
"
"  [Blog2]
"  blog_url = https://somebody.wordpress.com/
"  username = someone
"  password_cmd = gpg --quiet -d ~/.config/vimblog/somebody.gpg
"
" #######################################################################

" MAIN

" Check for Python
if !has("python")
    " silent! echoerr 'VimBlog ERROR: VimBlog requires Vim to be compiled with +python'
    finish
endif

" Import Python code
execute "python import sys"
execute "python sys.path.append(r'" . expand("<sfile>:p:h") . "')"
execute "python import blog"

" Define completion functions for commands
function! CompSave(ArgLead, CmdLine, CursorPos)
  return "publish\ndraft\n"
endfunction

function! CompPrev(ArgLead, CmdLine, CursorPos)
  return "local\npublish\ndraft\n"
endfunction

function! CompEditType(ArgLead, CmdLine, CursorPos)
  return "post\npage\n"
endfunction

" Define commands
command! -nargs=? -complete=custom,CompEditType BlogList silent execute('py blog.blog_list(<f-args>)')
command! -nargs=? -complete=custom,CompEditType BlogNew silent execute('py blog.blog_new(<f-args>)')
command! -nargs=? -complete=custom,CompSave BlogSave silent execute('py blog.blog_save(<f-args>)')
command! -nargs=? -complete=custom,CompPrev BlogPreview silent execute('py blog.blog_preview(<f-args>)')
command! -nargs=1 -complete=file BlogUpload silent execute('py blog.blog_upload_media(<f-args>)')
command! -nargs=1 BlogOpen silent execute('py blog.blog_guess_open(<f-args>)')
command! -nargs=? BlogSwitch silent execute('py blog.blog_config_switch(<f-args>)')
command! -nargs=? BlogCode silent execute('py blog.blog_append_code(<f-args>)')


